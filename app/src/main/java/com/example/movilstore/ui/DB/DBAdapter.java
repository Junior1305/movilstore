package com.example.movilstore.ui.DB;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.movilstore.ui.Entidad.Noticia;

import java.util.ArrayList;
import java.util.List;

public class DBAdapter extends SQLiteOpenHelper {

    private static final String NOMBRE_BD="noticia.bd";
    private static final int VERSION_BD=1;
    private static final String TABLA_NOTICIA="CREATE TABLE NOTICIA(CODIGO INTEGER PRIMARY KEY AUTOINCREMENT,USERID TEXT,TITLE TEXT, DESCRIPTION TEXT, FECHA TEXT)";

    public DBAdapter(Context context) {
        super(context, NOMBRE_BD,null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLA_NOTICIA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS "+TABLA_NOTICIA);
        sqLiteDatabase.execSQL(TABLA_NOTICIA);
    }

    public void agregarNoticia(String userid,String title, String description, String fecha){
        SQLiteDatabase bd=getWritableDatabase();
        if(bd!=null){
            bd.execSQL("INSERT INTO NOTICIA (USERID,TITLE,DESCRIPTION,FECHA) VALUES('"+userid+"','"+title+"','"+description+"','"+fecha+"')");
            bd.close();
        }
    }

    public List<Noticia> mostrarNoticias() {
        List<Noticia> noticias = new ArrayList<>();
        noticias.add(new Noticia(1,"1","Noticia 1","Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum","2020-05-13"));
        noticias.add(new Noticia(2,"1","Noticia 2","Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum","2020-05-14"));
        noticias.add(new Noticia(3,"1","Noticia 3","Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum","2020-05-15"));
        noticias.add(new Noticia(4,"1","Noticia 4","Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum","2020-05-16"));
        noticias.add(new Noticia(5,"1","Noticia 5","Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum","2020-05-17"));
        noticias.add(new Noticia(6,"1","Noticia 6","Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum","2020-05-18"));
        noticias.add(new Noticia(7,"1","Noticia 7","Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum Lorep Ipsum","2020-05-19"));
        return noticias;
    }

    public void eliminarNoticias(int id){
        SQLiteDatabase bd = getWritableDatabase();
        if(bd!= null){
            bd.execSQL("DELETE FROM NOTICIA WHERE CODIGO="+id);
            bd.close();
        }
    }
}
