package com.example.movilstore.ui.account;

import android.app.AlertDialog;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.movilstore.LoadingScreen;
import com.example.movilstore.R;
import com.example.movilstore.ui.Session;
import com.example.movilstore.ui.task.LoginTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.ExecutionException;

public class LoginFragment extends Fragment {

    private EditText txtEmail,txtPass;
    private Session session;


    final DialogFragment loadingScreen = LoadingScreen.getInstance();
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        Button btn_return_login = (Button) root.findViewById(R.id.btn_return_login);
        txtEmail = (EditText) root.findViewById(R.id.txt_email_login);
        txtPass = (EditText)root.findViewById(R.id.txt_contrasena_login);

        btn_return_login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_container, new AccountFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button lnk_olvidaste_contrasena = (Button) root.findViewById(R.id.lnk_olvidaste_contrasena);
        lnk_olvidaste_contrasena.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_container, new OlvidasteContrasenaFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        Button btn_ingresar_login = (Button) root.findViewById(R.id.btn_registro2);
        btn_ingresar_login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                session = new Session(getContext());
                session.setuserid("1");
                session.setusername("JUNIOR HUALLULLO");
                vaciarDataLogin();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.nav_container, new UserFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        return root;
    }



    public boolean validateInputsLogin() {
        Boolean valor = true;
        String resultado = "";

        if(txtEmail.getText().toString().matches("")){
            resultado = resultado + "Ingrese su email \n";
        }

        if(txtPass.getText().toString().matches("")){
            resultado = resultado + "Ingrese su password";
        }

        if(resultado == "")
        {
            valor = true;
        }else{
            Toast toast1 =
                    Toast.makeText(getContext(),
                            resultado, Toast.LENGTH_LONG);

            toast1.show();
            valor = false;
        }
        return valor;
    }

    public void vaciarDataLogin() {
        txtEmail.setText("");
        txtPass.setText("");
    }
}
