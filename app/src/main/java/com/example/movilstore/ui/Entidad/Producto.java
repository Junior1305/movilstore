package com.example.movilstore.ui.Entidad;

public class Producto {
    private String Foto;
    private String productoid;
    private String titulo;
    private String descripcion;
    private String precio;

    public Producto(String productoid, String foto, String titulo, String descripcion, String precio) {
        Foto = foto;
        this.productoid = productoid;
        this.titulo = titulo;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public void setFoto(String foto) {
        Foto = foto;
    }

    public void setProductoid(String productoid) {
        this.productoid = productoid;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getFoto() {
        return Foto;
    }

    public String getProductoid() { return productoid;}

    public String getTitulo() {
        return titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getPrecio() {
        return precio;
    }
}
